package com.pragma.monolito.aplicacion.persona.controlador;


import com.pragma.monolito.dominio.persona.entidad.Persona;
import com.pragma.monolito.infraestructura.persona.servicio.implementacion.PersonaServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/personas")
public class PersonaController {

    @Autowired
    private PersonaServiceImpl personaService;

    @Operation(summary = "Mostrar todas las personas")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Personas Encontradas", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Persona.class)) }),
            @ApiResponse(responseCode = "400", description = "Personas Invalidas", content = @Content),
            @ApiResponse(responseCode = "404", description = "Personas no encontradas", content = @Content),
            @ApiResponse(responseCode = "500", description = "Error inesperado", content = @Content)})
    @GetMapping
    public ResponseEntity<List<Persona>> getPersonas(){
        List<Persona> personas = personaService.listarPersonas();
        if (personas.isEmpty()){
            return ResponseEntity.noContent().build();
        }else{
            return ResponseEntity.ok(personas);
        }
    }

    @Operation(summary = "Guardar una persona")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Persona guardada correctamente", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Persona.class)) }),
            @ApiResponse(responseCode = "400", description = "Informacion invalida", content = @Content),
            @ApiResponse(responseCode = "404", description = "Persona no guardada", content = @Content),
            @ApiResponse(responseCode = "500", description = "Error inesperado", content = @Content)})
    @PostMapping
    public ResponseEntity<Persona> savePersona(@RequestBody Persona p){
        Persona persona = personaService.guardarPersona(p);
        return ResponseEntity.ok(persona);
    }

    @Operation(summary = "Buscar una Persona por su ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Persona encontrada", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Persona.class)) }),
            @ApiResponse(responseCode = "400", description = "Informacion invalida", content = @Content),
            @ApiResponse(responseCode = "404", description = "Persona no encontrada", content = @Content),
            @ApiResponse(responseCode = "500", description = "Error inesperado", content = @Content)})
    @GetMapping("/{idPersona}")
    public ResponseEntity<Persona> getPersonaById(@PathVariable("idPersona") Long idPersona){
        Persona persona = personaService.encontrarPersonabyId(idPersona);
        if(persona == null){
            return ResponseEntity.notFound().build();
        }else{
            return ResponseEntity.ok(persona);
        }
    }

    @Operation(summary = "Eliminar una persona")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Persona eliminada ", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Persona.class)) }),
            @ApiResponse(responseCode = "400", description = "Informacion invalida", content = @Content),
            @ApiResponse(responseCode = "404", description = "Persona no eliminada ", content = @Content),
            @ApiResponse(responseCode = "500", description = "Error inesperado", content = @Content)})
    @DeleteMapping("/delete")
    public ResponseEntity<Persona> deletePersona(@RequestBody Persona persona){

        Persona persona1 = personaService.eliminarPersona(persona);

        if(persona1==null){
            return ResponseEntity.notFound().build();
        }else{
            return ResponseEntity.ok(persona1);
        }
    }

    @Operation(summary = "Actualizar a una persona")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Persona actualizada correctamente", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Persona.class)) }),
            @ApiResponse(responseCode = "400", description = "Informacion invalida", content = @Content),
            @ApiResponse(responseCode = "404", description = "Persona no actualizada", content = @Content),
            @ApiResponse(responseCode = "500", description = "Error inesperado", content = @Content)})
    @PatchMapping("/{idPersona}")
    public ResponseEntity<Persona> updatePersona(@PathVariable("idPersona") Long idPersona,@RequestBody Persona p){
        Persona persona= personaService.actualizarPersona(idPersona,p);
        if(persona==null){
           return ResponseEntity.notFound().build();
        }else{
           return ResponseEntity.ok(persona);
        }
    }
}
