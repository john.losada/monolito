package com.pragma.monolito.aplicacion.imagen.controlador;


import com.pragma.monolito.dominio.imagen.entidad.Imagen;
import com.pragma.monolito.dominio.persona.entidad.Persona;
import com.pragma.monolito.infraestructura.imagen.servicio.implementacion.ImagenServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/imagenes")
public class ImagenController {

    @Autowired
    private ImagenServiceImpl imagenService;

    @Operation(summary = "Mostrar todas las imagenes")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Imagenes Encontradas", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Imagen.class)) }),
            @ApiResponse(responseCode = "400", description = "Imagenes Invalidas", content = @Content),
            @ApiResponse(responseCode = "404", description = "Imagenes no encontradas", content = @Content),
            @ApiResponse(responseCode = "500", description = "Error inesperado", content = @Content)})
    @GetMapping
    public ResponseEntity<List<Imagen>> getImagenes(){
        List<Imagen> imagenes = imagenService.listarImagenes();
        if (imagenes.isEmpty()){
            return ResponseEntity.noContent().build();
        }else{
            return ResponseEntity.ok(imagenes);
        }
    }

    @Operation(summary = "Guardar una Imagen")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Imagen guardada correctamente", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Imagen.class)) }),
            @ApiResponse(responseCode = "400", description = "Informacion invalida", content = @Content),
            @ApiResponse(responseCode = "404", description = "Imagen no guardada", content = @Content),
            @ApiResponse(responseCode = "500", description = "Error inesperado", content = @Content)})
    @PostMapping
    public ResponseEntity<Imagen> saveImagen(@RequestParam(value="idPersona") Long idPersona, @RequestParam(value = "bytes")MultipartFile mpf){
       Imagen imagen= imagenService.guardarImagen(mpf,idPersona);
        return ResponseEntity.ok(imagen);
    }

    @Operation(summary = "Eliminar una imagen por su id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Imagen eliminada ", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Imagen.class)) }),
            @ApiResponse(responseCode = "400", description = "Informacion invalida", content = @Content),
            @ApiResponse(responseCode = "404", description = "Imagen no eliminada ", content = @Content),
            @ApiResponse(responseCode = "500", description = "Error inesperado", content = @Content)})
    @DeleteMapping("/delete/{idImagen}")
    public ResponseEntity<Imagen> deleteImagen(@PathVariable("idImagen")String id){

        Imagen imagen = imagenService.eliminarImagen(id);
        if(imagen == null){
            return ResponseEntity.notFound().build();
        }else{
            return ResponseEntity.ok(imagen);
        }
    }
    @Operation(summary = "Actualizar a una imagen")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Imagen actualizada correctamente", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Imagen.class)) }),
            @ApiResponse(responseCode = "400", description = "Informacion invalida", content = @Content),
            @ApiResponse(responseCode = "404", description = "Imagen no actualizada", content = @Content),
            @ApiResponse(responseCode = "500", description = "Error inesperado", content = @Content)})
    @PatchMapping("/{idImagen}")
    public ResponseEntity<Imagen> updateImagen(@PathVariable("idImagen") String id, @RequestParam(value = "bytes")MultipartFile mpf){
        Imagen imagen= imagenService.actualizarImagen(id,mpf);
        if(imagen==null){
            return ResponseEntity.notFound().build();
        }else{
            return ResponseEntity.ok(imagen);
        }
    }

    @Operation(summary = "Buscar una Imagen por su ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Imagen encontrada", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Persona.class)) }),
            @ApiResponse(responseCode = "400", description = "Informacion invalida", content = @Content),
            @ApiResponse(responseCode = "404", description = "Imagen no encontrada", content = @Content),
            @ApiResponse(responseCode = "500", description = "Error inesperado", content = @Content)})
    @GetMapping("/{idImagen}")
    public ResponseEntity<Imagen> getImagenById(@PathVariable("idImagen") String id){
        Imagen imagen = imagenService.getImagenById(id);
        if(imagen == null){
            return ResponseEntity.notFound().build();
        }else{
            return ResponseEntity.ok(imagen);
        }
    }

    @Operation(summary = "Obtener la imagen en su respectivo formato")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Imagen encontrada", content = { @Content(mediaType = MediaType.IMAGE_JPEG_VALUE, schema = @Schema(implementation = byte.class)) }),
            @ApiResponse(responseCode = "400", description = "Informacion invalida", content = @Content),
            @ApiResponse(responseCode = "404", description = "Imagen no encontrada", content = @Content),
            @ApiResponse(responseCode = "500", description = "Error inesperado", content = @Content)})
    @GetMapping(value = "imagen/{id}", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> getImagenBytes(@PathVariable("id")String id){
        byte[] imagen =imagenService.getImagenByte(id);
        if(imagen == null){
            return ResponseEntity.notFound().build();
        }else{
            return ResponseEntity.ok(imagen);
        }

    }

}
