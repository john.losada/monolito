package com.pragma.monolito.dominio.imagen.entidad;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Document(collection = "imagen")
public class Imagen implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    private byte[] bytes;

    private Long idPersona;



}
