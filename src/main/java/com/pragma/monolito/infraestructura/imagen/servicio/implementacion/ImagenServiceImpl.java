package com.pragma.monolito.infraestructura.imagen.servicio.implementacion;

import com.pragma.monolito.dominio.imagen.entidad.Imagen;
import com.pragma.monolito.infraestructura.imagen.servicio.ImagenService;
import com.pragma.monolito.infraestructura.imagen.repositorio.ImagenRepository;
import com.pragma.monolito.infraestructura.excepciones.NoDataFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Service
public class ImagenServiceImpl implements ImagenService {

    @Autowired
    private ImagenRepository imagenRepository;

    @Override
    @Transactional
    public List<Imagen> listarImagenes(){

        List<Imagen> imagenes= imagenRepository.findAll();
        if (imagenes!=null){
            return imagenes;
        }else{
            throw new NoDataFoundException("Las imagenes no pudieron ser cargadas correctamente");
        }

    }

    @Override
    @Transactional
    public Imagen guardarImagen(MultipartFile mpf,Long idPersona){

        Imagen imagen = new Imagen();
        UUID uuid=UUID.randomUUID();
        try {

            imagen.setId(uuid.toString());
            imagen.setBytes(mpf.getBytes());
            imagen.setIdPersona(idPersona);

        } catch (IOException e) {
            throw new RuntimeException("La imagen no pudo ser guardada correctamente: "+e.getMessage());
        }

        return imagenRepository.save(imagen);

    }

    @Override
    @Transactional
    public Imagen actualizarImagen(String id,MultipartFile mpf){

        Imagen imagen= imagenRepository.findById(id).orElse(null);
        if(imagen!=null){
            try {
                imagen.setBytes(mpf.getBytes());
                imagen.setIdPersona(Long.valueOf(id));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            imagenRepository.save(imagen);
            return imagen;
        }else{
            throw new RuntimeException("La imagen no pudo ser actualizada correctamente la imagen");
        }

    }

    @Override
    @Transactional
    public Imagen getImagenById(String id){

        Imagen imagen = imagenRepository.findById(id).orElse(null);

        if(imagen!=null){
            return imagen;
        }else{
            throw new NoDataFoundException("La imagen a actualizar con id "+id+" no fue encontrada");
        }



    }

    @Override
    @Transactional
    public Imagen eliminarImagen(String id){

       Imagen imagen= imagenRepository.findById(id).orElse(null);

        if(imagen!=null){
            imagenRepository.delete(imagen);
            return imagen;
        }else {
            throw new NoDataFoundException("La imagen a eliminar no fue encontrada");
        }

    }

    @Override
    @Transactional
    public byte[] getImagenByte(String id){
        Imagen imagen= imagenRepository.findById(id).orElse(null);
        if(imagen!=null ){
            return imagen.getBytes();
        }else{
            throw new NoDataFoundException("La imagen con id "+id+" no pudo ser procesada correctamente");
        }


    }


}