package com.pragma.monolito.infraestructura.imagen.servicio;

import com.pragma.monolito.dominio.imagen.entidad.Imagen;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ImagenService {

    public List<Imagen> listarImagenes();

    public Imagen guardarImagen(MultipartFile mpf, Long idPersona);

    public Imagen actualizarImagen(String id,MultipartFile mpf);

    public Imagen getImagenById(String id);

    public Imagen eliminarImagen(String id);

    public byte[] getImagenByte(String id);



}
