package com.pragma.monolito.infraestructura.imagen.repositorio;

import com.pragma.monolito.dominio.imagen.entidad.Imagen;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ImagenRepository extends MongoRepository<Imagen, String> {

}