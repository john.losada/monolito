package com.pragma.monolito.infraestructura.persona.repositorio;

import com.pragma.monolito.dominio.persona.entidad.Persona;
import org.springframework.data.repository.CrudRepository;

public interface PersonaRepository extends CrudRepository<Persona, Long> {
    
}
