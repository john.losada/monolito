package com.pragma.monolito.infraestructura.persona.servicio;

import java.util.List;

import com.pragma.monolito.dominio.persona.entidad.Persona;

public interface PersonaService {
    
    public List<Persona> listarPersonas();
    
    public Persona guardarPersona(Persona persona);
    
    public Persona eliminarPersona(Persona persona);
    
    public Persona encontrarPersonabyId(Long idPersona);

    public Persona actualizarPersona(Long idPersona, Persona persona);

}
