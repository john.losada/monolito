package com.pragma.monolito.infraestructura.persona.servicio.implementacion;

import java.util.List;

import com.pragma.monolito.infraestructura.excepciones.NoDataFoundException;
import com.pragma.monolito.infraestructura.persona.repositorio.PersonaRepository;
import com.pragma.monolito.dominio.persona.entidad.Persona;
import com.pragma.monolito.infraestructura.persona.servicio.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PersonaServiceImpl implements PersonaService {

    @Autowired
    private PersonaRepository personaRepository;
    
    @Override
    @Transactional(readOnly = true)
    public List<Persona> listarPersonas() {
        List<Persona> personas = (List<Persona>) personaRepository.findAll();
        if (personas!=null){
            return personas;
        }else{
            throw new NoDataFoundException("Las personas no se pudieron cargar correctamente");
        }

    }

    @Override
    @Transactional
    public Persona guardarPersona(Persona persona) {
        Persona p = personaRepository.save(persona);
        if(p!=null){
            return p;
        }else{
            throw new NoDataFoundException("No se pudo guardar a la persona correctamente");
        }

    }

    @Override
    @Transactional
    public Persona eliminarPersona(Persona persona) {

        persona= personaRepository.findById(persona.getIdPersona()).orElse(null);

        if(persona!=null){
             personaRepository.delete(persona);
            return persona;
        }else {
            throw new NoDataFoundException("La persona a eliminar no fue encontrada");
        }

    }

    @Override
    @Transactional(readOnly = true)
    public Persona encontrarPersonabyId(Long idPersona) {
        Persona persona = personaRepository.findById(idPersona).orElse(null);
        if (persona!=null){
            return persona;
        }else{
            throw new NoDataFoundException("La persona con id "+idPersona+" no fue encontrada");
        }

    }

    @Override
    @Transactional
    public Persona actualizarPersona(Long idPersona, Persona persona) {
        Persona p = personaRepository.findById(idPersona).orElse(null);
        if (p != null) {
            p.setNombre(persona.getNombre());
            p.setApellido(persona.getApellido());
            p.setEmail(persona.getEmail());
            p.setTelefono(persona.getTelefono());


            personaRepository.save(p);
            return p;
        } else {
            throw new NoDataFoundException("La persona a actualizar con id "+idPersona+" no fue encontrada");
        }
    }

}
