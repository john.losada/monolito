package com.pragma.monolito.infraestructura.excepciones;

public class NoDataFoundException extends RuntimeException{

    public NoDataFoundException(String mensaje){
        super(mensaje);
    }
}
